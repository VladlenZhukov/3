@echo off
setlocal enabledelayedexpansion
if [%1] == [] exit /b 9
if [%1] == [/?] goto hint
ECHO ### Checking the number of files under %1 ###
set numFiles=0
for %%x in (%1\*.*) do (
  set file[!numFiles!]=%%~nfx
  set /a numFiles+=1
 ) 
ECHO ### Number of files found: %numFiles%

set /a index=%numFiles%-1
for /L %%i in (0,1,%index%) do (
  echo !file[%%i]!
  )
endlocal
exit /b 0

:hint
echo Counts the number of files in the given directory.
echo Example: count_file_in_subfolder source
exit /b 0