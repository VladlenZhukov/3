mkdir labs
cd labs
mkdir PZ-22u-1
cd PZ-22u-1
mkdir Zhukov_Vladlen
cd Zhukov_Vladlen
mkdir batch
cd batch
mkdir hide_dir
attrib +h hide_dir
mkdir non_hide_dir
cd non_hide_dir
xcopy /?
xcopy /? > copyhelp.txt
cd ..
echo f | xcopy /f /y non_hide_dir\copyhelp.txt hide_dir\copied_copyhelp.txt